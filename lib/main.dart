import 'package:flutter/material.dart';
import 'package:todoolist/screens/tasks_screen.dart';
import 'package:provider/provider.dart';
import 'package:todoolist/models/task_data.dart';

void main()  {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      // create: (context) => TaskData(),
      create: (BuildContext context) {
        return TaskData();
      },
      child: MaterialApp(
      home: TasksScreen(),
      ),
    );
  }
}




