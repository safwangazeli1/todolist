import 'package:flutter/material.dart';
import 'package:todoolist/models/task.dart';
import 'package:todoolist/screens/tasks_screen.dart';
import 'package:provider/provider.dart';
import 'package:todoolist/models/task_data.dart';

class AddTaskScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    String newTaskTitle = "Try again";

    return Container(
      color: Color(0xff757575),
      child: Container(
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20.0),
            topLeft: Radius.circular(20.0),
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('Add Task',
              textAlign: TextAlign.center,
              style: TextStyle(
              fontSize: 30.0,
              color: Colors.lightBlueAccent,
              ),
            ),
            TextField(
              autofocus: true,
              textAlign: TextAlign.center,
              onChanged: (newText) {
                  // print(newText);
                  newTaskTitle = newText;
              } ,
            ),
            FlatButton(
              child: Text('Add', style: TextStyle(
                color: Colors.white,
                ),
              ),
              color: Colors.lightBlueAccent,
              onPressed: () {
                 Provider.of<TaskData>(context, listen: false).addTask(newTaskTitle);
                 Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
