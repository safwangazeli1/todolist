# About Todolist

This is a flutter todo list project which allows users to create, read, update and delete all the tasks. This project uses Firebase as a database.
